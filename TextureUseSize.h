/// @file TextureSize.h
/// @brief テクスチャの使用領域サイズを保持するクラス - 定義
/// @note 
/// @date 2012/11/28     tory

//====================================================================
//             TextureUseSize.h
//--------------------------------------------------------------------
//    処理内容 : テクスチャの使用領域サイズを保持するクラス - 定義
//    処理内容 : テクスチャの使用する幅のwidth,heightを保持する
//    作成詳細 : 2012/11/28     tory
//    補足     : 
//    追記内容 : 
//    作成詳細 : 
//
//    追記内容 : 
//    作成詳細 : 2011/09/01     tory
//         
//====================================================================

#ifndef INCLUDE_TORYLIB_TEXTURE_USE_SIZE_H
#define INCLUDE_TORYLIB_TEXTURE_USE_SIZE_H

#include "Common\UV_Rect.h"     // UV、TextureSize用


namespace ToryLib{
    namespace Graphics{

        //----------------------------------------------------------
        // プロトタイプ宣言
        // ここで使用する変数の型を指定すると全てに適用される
        typedef unsigned int T;


        /// @class TextureSize
        /// @brief テクスチャの使用領域サイズを保持するクラス
        //template <class T>
        //class TextureUseSize : public Common::WH<T>{
        class TextureUseSize : public Common::WH<T>{
            //----------------------------------------------------------
            // 特殊メンバ関数
        public:

            /// @brief コンストラクタ
            TextureUseSize::TextureUseSize(){
                this->set(0, 0);
            }

            /// @brief コンストラクタ
            TextureUseSize::TextureUseSize(const T a_Width, const T a_Height){
                this->set(a_Width, a_Height);
            }


            /// @brief デストラクタ
            virtual TextureUseSize::~TextureUseSize();

            //----------------------------------------------------------
            // メンバ変数
        private:

            //----------------------------------------------------------
            // メンバ関数
        public:

            /// @brief テクスチャの使用領域サイズを設定します。
            void TextureUseSize::set(const T a_Width, const T a_Height){
                this->width = a_Width; 
                this->height= a_Height;
            }

 
            /// @brief テクスチャの使用領域のサイズを取得します。
            const TextureUseSize TextureUseSize::get(){
                return *this;
            }
            
            

        }; // end of class TextureUseSize
           

    }    // end of namespace Graphics
}    // end of namespace ToryLib

#endif